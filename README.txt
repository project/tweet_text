CKEditor Tweetable Text -  is an extension to the Drupal 8 CKEditormodule.

Provides an input filter and WYSIWYG plugin for making text in content tweetable, just by clicking on the sentance or phrase itself! . 


REQUIREMENTS
============
- ckeditor


INSTALLATION
============
1- Download the tweetable_text folder to your modules directory.
2- Copy the tweet_text folder into your modules folder and place the tweetable_text folder in the root libraries folder (/libraries).
3- Go to admin/modules and enable the module.
4- Go to admin/config/content/formats and configure the desired profile.
5- Move a button into the Active toolbar.
6- Clear your browser's cache, and a new Twitter button will appear in your toolbar.


MODULE DEVELOPERS
=================
Module created by :
1- Vishal Gupta ( https://www.drupal.org/u/vishal9619 )

MAINTAINERS
===========
Module maintained by :
1- Gaurav Kapoor ( https://www.drupal.org/u/gauravkapoor )